library ieee;
USE ieee.std_logic_1164.all;

entity multi is
  port(clk       : IN  STD_LOGIC;
       boot      : IN  STD_LOGIC;
       ldpc_l    : IN  STD_LOGIC;
       wrd_l     : IN  STD_LOGIC;
       wr_m_l    : IN  STD_LOGIC;
       w_b       : IN  STD_LOGIC;
       ldpc      : OUT STD_LOGIC;
       wrd       : OUT STD_LOGIC;
       wr_m      : OUT STD_LOGIC;
       ldir      : OUT STD_LOGIC;
       ins_dad   : OUT STD_LOGIC;
       word_byte : OUT STD_LOGIC;
       int_en    : IN  STD_LOGIC;
       int_rq    : IN  STD_LOGIC;
	   enter_sys : OUT STD_LOGIC);
end entity;

architecture Structure of multi is

  -- Aqui iria la declaracion de las los estados de la maquina de estados
  type state_type is (FETCH, DEMW, SYSTEM);
  signal estado, next_estado : state_type;
  --signal act_pc : std_logic;
  
begin

  -- Aqui iria la m�quina de estados del modelos de Moore que gestiona el multiciclo
  -- Aqui irian la generacion de las senales de control que su valor depende del ciclo en que se esta.
  
  ldpc <= ldpc_l when next_estado = FETCH else '0';
  next_estado <= FETCH when (estado = SYSTEM or (estado = DEMW and (int_en = '0' or (int_en = '1' and int_rq = '0')))) else
                 DEMW when estado = FETCH else
                 SYSTEM when (estado = DEMW and int_en = '1' and int_rq = '1');
  wrd <= wrd_l when estado = DEMW else '0';
  wr_m <= wr_m_l when estado = DEMW else '0';
  word_byte <= w_b when estado = DEMW else '0';
  ins_dad <= '1' when estado = DEMW else '0';
  ldir <= '1' when estado = FETCH else '0';
  enter_sys <= '1' when estado = SYSTEM else '0';
  
  process(clk, boot)
  begin
    if boot = '1' then
      estado <= FETCH;
    elsif rising_edge(clk) then
      estado <= next_estado;
    end if;
  end process;
  
end Structure;
