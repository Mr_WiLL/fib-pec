onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/boot
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/clk
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/datard_m
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/op
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/wrd
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/addr_a
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/addr_b
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/addr_d
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/immed
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/pc
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/ins_dad
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/in_d
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/immed_x2
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/wr_m
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/rb_n
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/word_byte
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/aluout
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/z
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/pc_act
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/addr_io
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/rd_in
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/wr_out
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/quirk
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/a_sys
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/d_sys
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/int_en
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/int_rq
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/enter_sys
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/inta
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/next_jmp_pc
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/next_br_pc
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/next_pc
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/new_pc
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/ir_reg
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/act_pc
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/act_ir
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/Iop
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/Iwr_m
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/Ildpc
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/Iwrd
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/Iwb
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/tknbr
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/Ienter_sys
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/Iinta
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/jz_jump
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/jnz_jump
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/bz_jump
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/bnz_jump
add wave -noupdate -expand -group ucontrol -radix hexadecimal /test_sisa/SoC/pro0/c0/save_jmp_pc
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/clk
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/boot
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/ldpc_l
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/wrd_l
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/wr_m_l
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/w_b
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/ldpc
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/wrd
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/wr_m
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/ldir
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/ins_dad
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/word_byte
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/int_en
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/int_rq
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/enter_sys
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/estado
add wave -noupdate -expand -group multi -radix hexadecimal /test_sisa/SoC/pro0/c0/m0/next_estado
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {100 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1 ns}
