LIBRARY ieee,work;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE work.alucodes.all;

ENTITY alu IS
  PORT (x  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        y  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        op : IN  aluop;
        w  : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        z  : OUT STD_LOGIC);
END alu;


ARCHITECTURE Structure OF alu IS
  signal resadd, ressub : STD_LOGIC_VECTOR(15 DOWNTO 0);
  signal l, rl, ra, resshl, ressha : STD_LOGIC_VECTOR(15 DOWNTO 0);
  signal shnum_fmt : NATURAL;
  signal shnum : STD_LOGIC_VECTOR(4 DOWNTO 0);
  signal is_lt, is_le, is_eq, is_ltu, is_leu : STD_LOGIC;
  signal resmuls, resmulu : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal resdivs, resdivu : STD_LOGIC_VECTOR(15 DOWNTO 0);
  signal v_w : STD_LOGIC_VECTOR(15 DOWNTO 0);

  function std_logic_compare(result: boolean) return std_logic is
  begin
    if result then return '1';
    else return '0';
    end if;
  end function std_logic_compare;
BEGIN

  -- Aqui iria la definicion del comportamiento de la ALU
  
  -- logica de adiciones
  resadd <= std_logic_vector(unsigned(x) + unsigned(y));
  ressub <= std_logic_vector(unsigned(x) - unsigned(y));
  
  -- logica de shifts
  shnum <= y(4 downto 0);
  shnum_fmt <= to_integer(unsigned(std_logic_vector(abs(signed(shnum)))));
  l <= std_logic_vector(shift_left(unsigned(x), shnum_fmt));
  rl <= std_logic_vector(shift_right(unsigned(x), shnum_fmt));
  ra <= std_logic_vector(shift_right(signed(x), shnum_fmt));
  resshl <= rl when signed(shnum) < 0 else l;
  ressha <= ra when signed(shnum) < 0 else l;

  -- logica de comparaciones
  is_lt <= std_logic_compare(signed(x) < signed(y));
  is_ltu <= std_logic_compare(unsigned(x) < unsigned(y));
  is_le <= std_logic_compare(signed(x) <= signed(y));
  is_leu <= std_logic_compare(unsigned(x) <= unsigned(y));
  is_eq <= std_logic_compare(signed(x) = signed(y));

  -- logica de multiplicaciones
  resmuls <= std_logic_vector(signed(x) * signed(y));
  resmulu <= std_logic_vector(unsigned(x) * unsigned(y));

  -- logica de divisiones
  resdivs <= std_logic_vector(signed(x) / signed(y));
  resdivu <= std_logic_vector(unsigned(x) / unsigned(y));

  -- seleccion de resultado
  with op select v_w <= x and y when OPAND,
                        x or y when OPOR,
                        x xor y when OPXOR,
                        not x when OPNOT,
                        resadd when OPADD,
                        ressub when OPSUB,
                        ressha when OPSHA,
                        resshl when OPSHL,
                        (15 downto 1 => '0') & is_lt when OPCMPLT,
                        (15 downto 1 => '0') & is_le when OPCMPLE,
                        (15 downto 1 => '0') & is_eq when OPCMPEQ,
                        (15 downto 1 => '0') & is_ltu when OPCMPLTU,
                        (15 downto 1 => '0') & is_leu when OPCMPLEU,
                        resadd when OPADDI,
                        resadd when OPLD,
                        resadd when OPST,
                        resadd when OPLDB,
                        resadd when OPSTB,
                        y when OPMOVI,
                        y(7 downto 0) & x(7 downto 0) when OPMOVHI,
                        resmuls(15 downto 0) when OPMUL,
                        resmuls(31 downto 16) when OPMULH,
                        resmulu(31 downto 16) when OPMULHU,
                        resdivs when OPDIV,
                        resdivu when OPDIVU,
                        x when OPJMP,
                        x when OPJZ,
                        x when OPJNZ,
                        x when OPJAL,
                        x when OPRDS,
                        x when OPWRS,
                        x when OPRETI,
                        (others => 'X') when others;

  -- asignacion de salidas
  z <= std_logic_compare(unsigned(y) = 0);
  w <= v_w;
  
END Structure;
