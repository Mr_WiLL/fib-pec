LIBRARY ieee;
USE ieee.std_logic_1164.all;

package alucodes is
  type aluop is array (4 downto 0) of std_logic;
  constant OPAND, OPOR, OPXOR, OPNOT, OPADD, OPSUB, OPSHA, OPSHL : aluop;
  constant OPCMPLT, OPCMPLE, OPCMPEQ, OPCMPLTU, OPCMPLEU : aluop;
  constant OPADDI : aluop;
  constant OPLD, OPST, OPLDB, OPSTB : aluop;
  constant OPMOVI, OPMOVHI : aluop;
  constant OPMUL, OPMULH, OPMULHU, OPDIV, OPDIVU : aluop;
  constant OPHALT : aluop;
end package alucodes;

package body alucodes is
  constant OPAND	: aluop := "0" & x"0";
  constant OPOR		: aluop := "0" & x"1";
  constant OPXOR	: aluop := "0" & x"2";
  constant OPNOT	: aluop := "0" & x"3";
  constant OPADD	: aluop := "0" & x"4";
  constant OPSUB	: aluop := "0" & x"5";
  constant OPSHA	: aluop := "0" & x"6";
  constant OPSHL	: aluop := "0" & x"7";
  constant OPCMPLT	: aluop := "0" & x"8";
  constant OPCMPLE	: aluop := "0" & x"9";
  constant OPCMPEQ      : aluop := "0" & x"A";
  constant OPCMPLTU     : aluop := "0" & x"B";
  constant OPCMPLEU     : aluop := "0" & x"C";
  constant OPADDI	: aluop := "0" & x"D";
  constant OPLD		: aluop := "0" & x"E";
  constant OPST		: aluop := "0" & x"F";
  constant OPLDB	: aluop := "1" & x"0";
  constant OPSTB	: aluop := "1" & x"1";
  constant OPMOVI	: aluop := "1" & x"2";
  constant OPMOVHI	: aluop := "1" & x"3";
  constant OPMUL	: aluop := "1" & x"4";
  constant OPMULH	: aluop := "1" & x"5";
  constant OPMULHU	: aluop := "1" & x"6";
  constant OPDIV	: aluop := "1" & x"7";
  constant OPDIVU	: aluop := "1" & x"8";
  constant OPHALT	: aluop := "1" & x"9";
end package body alucodes;
