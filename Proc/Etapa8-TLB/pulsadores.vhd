LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY pulsadores IS
  PORT (boot	 : IN std_logic;
		clk		 : IN std_logic;
		inta	 : IN std_logic;
		keys	 : IN std_logic_vector(3 downto 0);
		intr	 : OUT std_logic;
		read_key : OUT std_logic_vector(3 downto 0));
END pulsadores;

ARCHITECTURE Structure OF pulsadores IS
  signal int : std_logic := '0';
  signal last_keys : std_logic_vector(3 downto 0);
BEGIN

  intr <= int;
  read_key <= last_keys;

  PROCESS (boot,clk)
  BEGIN
    if boot = '1' then
      last_keys <= keys;
      int <= '0';
    else
      if rising_edge(clk) then
        if int = '1' and inta = '1' then
          int <= '0';
        end if;
        
        if last_keys /= keys and int = '0' then
          last_keys <= keys;
          int <= '1';
        end if;
      end if;
    end if;
  END PROCESS;
END Structure;
