LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all;

entity tlb is
  port (boot    : in  std_logic;
        clk     : in  std_logic;
		wre 	: in  std_logic; -- write enable
		wrv 	: in  std_logic; -- write virtual address
		wri	 	: in  std_logic_vector(3 downto 0); -- write index
        vpn_in  : in  std_logic_vector(3 downto 0);
        ppn_in  : in  std_logic_vector(3 downto 0);
        v_in    : in  std_logic;
        r_in    : in  std_logic;
		-- salidas
        ppn_out : out std_logic_vector(3 downto 0);
        v_out   : out std_logic;
        r_out   : out std_logic;
        prot    : out std_logic;
        miss    : out std_logic);
end tlb;

architecture Structure of tlb is
  type TLB_ENTRY is record
    vpn : std_logic_vector(3 downto 0);
    ppn : std_logic_vector(3 downto 0);
    v   : std_logic; -- valid
    r   : std_logic; -- read only
    p   : std_logic; -- protected
  end record TLB_ENTRY;

  function std_logic_compare(result: boolean) return std_logic is
  begin
    if result then return '1';
    else return '0';
    end if;
  end function std_logic_compare;

  function find_first_bit(vector: std_logic_vector) return integer is
    variable i: integer;
  begin
    for i in vector'range loop
      if vector(i) = '1' then
        return i;
      end if;
    end loop;
    return -1;
  end function;
  
  type TLB_ENTRIES is array (7 downto 0) of TLB_ENTRY;
  signal entry : TLB_ENTRIES;
  signal cmp_set : std_logic_vector(7 downto 0) := x"00";
  signal cmp_pos : integer;
  signal miss_tmp : std_logic;
begin
  cmp_gen: for i in 0 to 7 generate
    cmp_set(i) <= std_logic_compare(entry(i).vpn = vpn_in);
  end generate;
  
  cmp_pos <= find_first_bit(cmp_set);

  miss_tmp <= std_logic_compare(unsigned(cmp_set) = 0);
  miss <= miss_tmp when wre = '0' else '0';

  ppn_out <= entry(cmp_pos).ppn when miss_tmp = '0' else (3 downto 0 => 'X');
  v_out <= entry(cmp_pos).v when miss_tmp = '0' else 'X';
  r_out <= entry(cmp_pos).r when miss_tmp = '0' else 'X';
  prot <= entry(cmp_pos).p when miss_tmp = '0' else 'X';
  
  process (boot, clk, wre, wrv, wri, vpn_in, ppn_in, v_in, r_in)
  begin
	if rising_edge(clk) then
	    if boot = '1' then
		  -- 0x0000 a 0x2FFF (páginas de usuario)
		  for i in 0 to 2 loop
			entry(i).vpn <= std_logic_vector(to_unsigned(i, 4));
			entry(i).ppn <= std_logic_vector(to_unsigned(i, 4));
			entry(i).v <= '1';
			entry(i).r <= '0';
			entry(i).p <= '0';
		  end loop;
		  
		  -- 0x8000 a 0x8FFF (páginas de sistema, parte baja de la RAM)
		  entry(3).vpn <= x"8";
		  entry(3).ppn <= x"8";
		  entry(3).v <= '1';
		  entry(3).r <= '0';
		  entry(3).p <= '1';
		
		  -- 0xC000 a 0xFFFF (páginas de sistema de sólo lectura)
		  for i in 4 to 7 loop
			entry(i).vpn <= std_logic_vector(to_unsigned(i, 4)+8);
			entry(i).ppn <= std_logic_vector(to_unsigned(i, 4)+8);
			entry(i).v <= '1';
			entry(i).r <= '1';
			entry(i).p <= '1';
		  end loop;
		 
		else
			if wre = '1' then
				if wrv = '1' then
					entry(to_integer(unsigned(wri))).vpn <= vpn_in;
				else
					entry(to_integer(unsigned(wri))).ppn <= ppn_in;
					entry(to_integer(unsigned(wri))).v <= v_in;
					entry(to_integer(unsigned(wri))).r <= r_in;
				end if;
			end if;
		end if;
	end if;
  end process;
end Structure;
