LIBRARY ieee,work;
USE ieee.std_logic_1164.all;
USE work.alucodes.all;

ENTITY proc IS
  PORT (clk       : IN  STD_LOGIC;
        boot      : IN  STD_LOGIC;
        datard_m  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        addr_m    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        data_wr   : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        wr_m      : OUT STD_LOGIC;
        word_byte : OUT STD_LOGIC;
        addr_io   : OUT STD_LOGIC_VECTOR(7 downto 0);
        rd_in	  : OUT STD_LOGIC;
        rd_io	  : IN  STD_LOGIC_VECTOR(15 downto 0);
        wr_out	  : OUT STD_LOGIC;
        wr_io	  : OUT STD_LOGIC_VECTOR(15 downto 0);
		intr      : IN  STD_LOGIC;
		inta 	  : OUT STD_LOGIC;
		inv_addr  : IN  STD_LOGIC;
		prot_addr : IN  STD_LOGIC);
END proc;

ARCHITECTURE Structure OF proc IS

  -- Aqui iria la declaracion de las entidades que vamos a usar
  -- Usaremos la palabra reservada COMPONENT ...
  -- Tambien crearemos los cables/buses (signals) necesarios para unir las entidades

  COMPONENT unidad_control IS
    PORT (boot      : IN  STD_LOGIC;
          clk       : IN  STD_LOGIC;
          datard_m  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          op        : OUT aluop;
          wrd       : OUT STD_LOGIC;
          addr_a    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_b    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_d    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
          immed     : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          pc        : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          ins_dad   : OUT STD_LOGIC;
          in_d      : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
          immed_x2  : OUT STD_LOGIC;
          wr_m      : OUT STD_LOGIC;
          word_byte : OUT STD_LOGIC;
          rb_n      : OUT STD_LOGIC;
          aluout    : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          z         : IN  STD_LOGIC;
          pc_act    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          addr_io   : OUT STD_LOGIC_VECTOR(7 downto 0);
          rd_in	    : OUT STD_LOGIC;
          wr_out    : OUT STD_LOGIC;
          quirk     : OUT STD_LOGIC_VECTOR(1 downto 0);
          a_sys     : OUT STD_LOGIC;
          d_sys     : OUT STD_LOGIC;
		  int_en	: IN  STD_LOGIC;
		  int_rq	: IN  STD_LOGIC;
		  enter_sys	: OUT STD_LOGIC;
          inta      : OUT STD_LOGIC;
          inv_addr  : IN  STD_LOGIC;
          div_z     : IN  STD_LOGIC;
          eid       : OUT STD_LOGIC_VECTOR(3 downto 0);
		  privilege : IN  STD_LOGIC;
		  prot_addr : IN  STD_LOGIC;
		wr_ins	  : OUT STD_LOGIC;
		wr_dat	  : OUT STD_LOGIC;
		wr_virt	  : OUT STD_LOGIC;
		wr_phys	  : OUT STD_LOGIC;
		miss_ins  : IN STD_LOGIC;
		miss_dat  : IN STD_LOGIC;
		v_ins_out : IN STD_LOGIC;
		v_dat_out : IN STD_LOGIC;
		prot_ins  : IN STD_LOGIC;
		prot_dat  : IN STD_LOGIC;
		r_dat_out : IN STD_LOGIC);
  END COMPONENT;
  
  COMPONENT datapath IS
    PORT (clk       : IN  STD_LOGIC;
          op 	    : IN  aluop;
          wrd       : IN  STD_LOGIC;
          addr_a    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_b    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_d    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          immed     : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          immed_x2  : IN  STD_LOGIC;
          datard_m  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          ins_dad   : IN  STD_LOGIC;
          pc        : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          in_d      : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
          rb_n	    : IN  STD_LOGIC;
          addr_m    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          data_wr   : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          aluout    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          z         : OUT STD_LOGIC;
          pc_act    : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          rd_io	    : IN  STD_LOGIC_VECTOR(15 downto 0);
          wr_io	    : OUT STD_LOGIC_VECTOR(15 downto 0);
          reset     : IN  STD_LOGIC;
          quirk     : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
          a_sys     : IN  STD_LOGIC;
          d_sys     : IN  STD_LOGIC;
          int_en    : OUT STD_LOGIC;
		  enter_sys : IN  STD_LOGIC;
          div_z     : OUT STD_LOGIC;
          eid       : IN  STD_LOGIC_VECTOR(3 downto 0);
		  privilege : OUT STD_LOGIC;
		wr_ins	  : IN STD_LOGIC;
		wr_dat	  : IN STD_LOGIC;
		wr_virt	  : IN STD_LOGIC;
		wr_phys	  : IN STD_LOGIC;
		miss_ins  : OUT STD_LOGIC;
		miss_dat  : OUT STD_LOGIC;
		v_ins_out : OUT STD_LOGIC;
		v_dat_out : OUT STD_LOGIC;
		prot_ins  : OUT STD_LOGIC;
		prot_dat  : OUT STD_LOGIC;
		r_dat_out : OUT STD_LOGIC);
  END COMPONENT;
  
  SIGNAL Iimmed_x2, Iins_dad, Iwrd, Irb_n, Iz, Ia_sys, Id_sys, Ienter_sys, Iint_en, Idiv_z, Iprivilege : std_logic;
  SIGNAL Iin_d, Iquirk : std_logic_vector(1 downto 0);
  SIGNAL Iop : aluop;
  SIGNAL Iaddr_a, Iaddr_b, Iaddr_d : std_logic_vector(2 downto 0);
  SIGNAL Ieid : std_logic_vector(3 downto 0);
  SIGNAL Iimmed, Ipc, Ialuout, Ipc_act : std_logic_vector(15 downto 0);
  SIGNAL Iwr_ins, Iwr_dat, Iwr_phys, Iwr_virt : std_logic;
  SIGNAL Imiss_ins, Imiss_dat, Iv_ins_out, Iv_dat_out, Iprot_ins, Iprot_dat, Ir_dat_out : std_logic;

BEGIN

  -- Aqui iria la declaracion del "mapeo" (PORT MAP) de los nombres de las entradas/salidas de los componentes
  -- En los esquemas de la documentacion a la instancia del DATAPATH le hemos llamado e0 y a la de la unidad de control le hemos llamado c0

  c0 : unidad_control PORT MAP(boot      => boot,
                               clk       => clk,
                               datard_m  => datard_m,
                               op        => Iop,
                               wrd       => Iwrd,
                               addr_a    => Iaddr_a,
                               addr_b    => Iaddr_b,
                               addr_d    => Iaddr_d,
                               immed     => Iimmed,
                               pc        => Ipc,
                               ins_dad   => Iins_dad,
                               in_d      => Iin_d,
                               immed_x2  => Iimmed_x2,
                               wr_m      => wr_m,
                               word_byte => word_byte,
                               rb_n 	 => Irb_n,
                               aluout    => Ialuout,
                               z         => Iz,
                               pc_act    => Ipc_act,
                               addr_io	 => addr_io,
                               rd_in 	 => rd_in,
                               wr_out	 => wr_out,
                               quirk     => Iquirk,
                               a_sys     => Ia_sys,
                               d_sys     => Id_sys,
                               int_en	 => Iint_en,
                               int_rq 	 => intr,
                               enter_sys => Ienter_sys,
                               inta 	 => inta,
                               inv_addr  => inv_addr,
                               div_z 	 => Idiv_z,
                               eid 		 => Ieid,
							   privilege => Iprivilege,
							   prot_addr => prot_addr,
								wr_ins => Iwr_ins,
								wr_dat => Iwr_dat,
								wr_virt => Iwr_virt,
								wr_phys => Iwr_phys,
								miss_ins => Imiss_ins,
								miss_dat => Imiss_dat,
								v_ins_out => Iv_ins_out,
								v_dat_out => Iv_dat_out,
								prot_ins => Iprot_ins,
								prot_dat => Iprot_dat,
								r_dat_out => Ir_dat_out);
  
  e0 : datapath PORT MAP(clk       => clk,
                         op        => Iop,
                         wrd       => Iwrd,
                         addr_a    => Iaddr_a,
                         addr_b    => Iaddr_b,
                         addr_d    => Iaddr_d,
                         immed     => Iimmed,
                         immed_x2  => Iimmed_x2,
                         datard_m  => datard_m,
                         ins_dad   => Iins_dad,
                         pc        => Ipc,
                         in_d      => Iin_d,
                         rb_n      => Irb_n,
                         addr_m    => addr_m,
                         data_wr   => data_wr,
                         aluout    => Ialuout,
                         z         => Iz,
                         pc_act    => Ipc_act,
                         rd_io	   => rd_io,
                         wr_io	   => wr_io,
                         reset     => boot,
                         quirk     => Iquirk,
                         a_sys     => Ia_sys,
                         d_sys     => Id_sys,
						 int_en	   => Iint_en,
						 enter_sys => Ienter_sys,
						 div_z 	   => Idiv_z,
						 eid	   => Ieid,
						 privilege => Iprivilege,
						wr_ins => Iwr_ins,
						wr_dat => Iwr_dat,
						wr_virt => Iwr_virt,
						wr_phys => Iwr_phys,
						miss_ins => Imiss_ins,
						miss_dat => Imiss_dat,
						v_ins_out => Iv_ins_out,
						v_dat_out => Iv_dat_out,
						prot_ins => Iprot_ins,
						prot_dat => Iprot_dat,
						r_dat_out => Ir_dat_out);
  
END Structure;
