LIBRARY ieee,work;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;
USE work.alucodes.all;

ENTITY datapath IS
  PORT (clk       : IN  STD_LOGIC;
        op        : IN  aluop;
        wrd       : IN  STD_LOGIC;
        addr_a    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_b    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_d    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
        immed     : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        immed_x2  : IN  STD_LOGIC;
        datard_m  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        ins_dad   : IN  STD_LOGIC;
        pc        : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        in_d      : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
        rb_n	  : IN  STD_LOGIC;
        addr_m    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        data_wr   : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        aluout    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        z         : OUT STD_LOGIC;
        pc_act    : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        rd_io	  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        wr_io	  : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        reset     : IN  STD_LOGIC;
        quirk     : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
        a_sys     : IN  STD_LOGIC;
        d_sys     : IN  STD_LOGIC;
		    int_en	  : OUT STD_LOGIC;
		    enter_sys : IN  STD_LOGIC;
        div_z     : OUT STD_LOGIC;
        eid       : IN STD_LOGIC_VECTOR(3 downto 0));
END datapath;


ARCHITECTURE Structure OF datapath IS
  
  COMPONENT regfile IS
    PORT (clk       : IN  STD_LOGIC;
          wrd       : IN  STD_LOGIC;
          d         : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          addr_a    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_b    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          addr_d    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
          a         : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          b         : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          d_sys     : IN  STD_LOGIC;
          a_sys     : IN  STD_LOGIC;
          reset     : IN  STD_LOGIC;
          quirk     : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
          enter_sys : IN STD_LOGIC;
		      int_en	: OUT STD_LOGIC;
          eid : IN STD_LOGIC_VECTOR(3 downto 0);
          e_addr : IN STD_LOGIC_VECTOR(15 downto 0));
  END COMPONENT;
  
  COMPONENT alu IS
    PORT (x  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          y  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
          op : IN  aluop;
          w  : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
          z  : OUT STD_LOGIC;
          div_z : OUT STD_LOGIC);
  END COMPONENT;
  
  SIGNAL seld, immfmt, rega, regb, wtod, mux_y, Iaddr_m, addr_reg : std_logic_vector(15 downto 0);
  
BEGIN
  
  with in_d select seld <= wtod when "00",
                           pc_act when "01",
                           rd_io when "10",
                           datard_m when others;

  immfmt <= shl(immed, (15 downto 1 => '0') & immed_x2);
  
  with rb_n select mux_y <= regb when '0',
                            immfmt when others;
  
  with ins_dad select Iaddr_m <= pc when '0',
                                wtod when others;
  addr_m <= Iaddr_m;

  data_wr <= regb;
  
  reg0 : regfile PORT MAP (clk => clk,
                           wrd => wrd,
                           d => seld,
                           addr_a => addr_a,
                           addr_b => addr_b,
                           addr_d => addr_d,
                           a => rega,
                           b => regb,
                           d_sys => d_sys,
                           a_sys => a_sys,
                           reset => reset,
                           quirk => quirk,
                           enter_sys => enter_sys,
						               int_en => int_en,
                           eid => eid,
                           e_addr => addr_reg);
  
  alu0 : alu PORT MAP (x => rega,
                       y => mux_y,
                       op => op,
                       w => wtod,
                       z => z,
                       div_z => div_z);

  aluout <= wtod;
  
  wr_io <= regb;
  
  PROCESS (clk)
  BEGIN
    if rising_edge(clk) then
      addr_reg <= Iaddr_m;
    end if;
  END PROCESS;

END Structure;
