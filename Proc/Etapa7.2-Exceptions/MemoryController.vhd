library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity MemoryController is
  port (CLOCK_50    : in  std_logic;
        addr        : in  std_logic_vector(15 downto 0);
        wr_data     : in  std_logic_vector(15 downto 0);
        rd_data     : out std_logic_vector(15 downto 0);
        we          : in  std_logic;
        byte_m      : in  std_logic;
        vga_addr    : out std_logic_vector(12 downto 0);
        vga_we      : out std_logic;
        vga_wr_data : out std_logic_vector(15 downto 0);
        vga_rd_data : in  std_logic_vector(15 downto 0);
        vga_byte_m  : out std_logic;
        -- señales para la placa de desarrollo
        SRAM_ADDR : out   std_logic_vector(17 downto 0);
        SRAM_DQ   : inout std_logic_vector(15 downto 0);
        SRAM_UB_N : out   std_logic;
        SRAM_LB_N : out   std_logic;
        SRAM_CE_N : out   std_logic := '1';
        SRAM_OE_N : out   std_logic := '1';
        SRAM_WE_N : out   std_logic := '1';
        inv_addr : out   std_logic);
end MemoryController;

architecture comportament of MemoryController is
  COMPONENT SRAMController is
    port (clk         : in    std_logic;
          SRAM_ADDR   : out   std_logic_vector(17 downto 0);
          SRAM_DQ     : inout std_logic_vector(15 downto 0);
          SRAM_UB_N   : out   std_logic;
          SRAM_LB_N   : out   std_logic;
          SRAM_CE_N   : out   std_logic := '1';
          SRAM_OE_N   : out   std_logic := '1';
          SRAM_WE_N   : out   std_logic := '1';
          address     : in    std_logic_vector(15 downto 0) := x"0000";
          dataRead    : out   std_logic_vector(15 downto 0);
          dataToWrite : in    std_logic_vector(15 downto 0);
          WR          : in    std_logic;
          byte_m      : in    std_logic := '0');
  end COMPONENT;
  
  SIGNAL sram_we : std_logic;
  SIGNAL mem_rd_data : std_logic_vector(15 downto 0);
  SIGNAL Iinv_addr : std_logic;

begin

  vga_addr <= addr(12 downto 0);
  vga_we <= we when addr >= x"A000" and addr < x"C000" and Iinv_addr = '0' else '0';
  vga_wr_data <= wr_data;
  vga_byte_m <= byte_m;

  Iinv_addr <= '1' when byte_m = '0' and addr(0) = '1' else '0';
  inv_addr <= Iinv_addr;

  sram_we <= we when addr < x"C000" and Iinv_addr = '0' else '0';
  
  rd_data <= vga_rd_data when addr >= x"A000" and addr < x"C000" and Iinv_addr = '0' else
             mem_rd_data when addr < x"A000" and Iinv_addr = '0' else
             (others => 'X');
  
  sram : SRAMController PORT MAP (clk => CLOCK_50,
                                  SRAM_ADDR => SRAM_ADDR,
                                  SRAM_DQ => SRAM_DQ,
                                  SRAM_UB_N => SRAM_UB_N,
                                  SRAM_LB_N => SRAM_LB_N,
                                  SRAM_CE_N => SRAM_CE_N,
                                  SRAM_OE_N => SRAM_OE_N,
                                  SRAM_WE_N => SRAM_WE_N,
                                  address => addr,
                                  dataRead => mem_rd_data,
                                  dataToWrite => wr_data,
                                  WR => sram_we,
                                  byte_m => byte_m);

  
end comportament;
