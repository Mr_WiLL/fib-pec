LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all; --Esta libreria sera necesaria si usais conversiones CONV_INTEGER
USE ieee.numeric_std.all;        --Esta libreria sera necesaria si usais conversiones TO_INTEGER

ENTITY regfile IS
  PORT (clk       : IN  STD_LOGIC;
        wrd       : IN  STD_LOGIC;
        d         : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
        addr_a    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_b    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
        addr_d    : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
        a         : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        b         : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        d_sys     : IN  STD_LOGIC;
        a_sys     : IN  STD_LOGIC;
        reset     : IN  STD_LOGIC;
        quirk     : IN  STD_LOGIC_VECTOR(1 downto 0); -- EI/DI/RETI/**
        enter_sys : IN STD_LOGIC;
		    int_en	  : OUT STD_LOGIC;
        eid    : IN  STD_LOGIC_VECTOR(3 downto 0);
        e_addr    : IN  STD_LOGIC_VECTOR(15 downto 0));
END regfile;

ARCHITECTURE Structure OF regfile IS
  type REG_ARR is array (7 downto 0) of std_logic_vector(15 downto 0);
  signal reg, sys_reg : REG_ARR;
  signal sys_reg_wr, reg_wr : std_logic;
  signal a_tmp0, a_tmp1 : std_logic_vector(15 downto 0);
BEGIN

  sys_reg_wr <= wrd and d_sys;
  reg_wr <= wrd and not d_sys;

  with a_sys select a_tmp0 <= reg(to_integer(unsigned(addr_a))) when '0',
                              sys_reg(to_integer(unsigned(addr_a))) when others;

  with quirk select a_tmp1 <= sys_reg(1) when "10", -- restaurar el PC guardado
                              a_tmp0 when others;

  with enter_sys select a <= sys_reg(5) when '1', -- cargar @RSG en PC
                             a_tmp1 when others;

  b <= reg(to_integer(unsigned(addr_b)));

  int_en <= sys_reg(7)(1);
  
  PROCESS (clk,reset)
  BEGIN
    if rising_edge(clk) then
		if reset = '1' then
		  sys_reg(7) <= x"0000";
		else
		  if reg_wr = '1' then
			reg(to_integer(unsigned(addr_d))) <= d;
		  elsif sys_reg_wr = '1' then
			sys_reg(to_integer(unsigned(addr_d))) <= d;
		  end if;
		  
		  case quirk is
			when "00" =>
			  sys_reg(7)(1) <= '1';
			when "01" =>
			  sys_reg(7)(1) <= '0';
			when "10" =>
			  sys_reg(7) <= sys_reg(0);
			when others =>
		  end case;

		  if enter_sys = '1' then
        sys_reg(0) <= sys_reg(7);
        sys_reg(1) <= d;
        sys_reg(2) <= x"000" & eid;
        if eid = x"1" then
          sys_reg(3) <= e_addr;
        end if;
        sys_reg(7)(1) <= '0';
        end if;
      end if;
    end if;
  END PROCESS;

END Structure;
