LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all;

ENTITY timer IS
  PORT (CLOCK_50 : IN std_logic;
		boot	 : IN std_logic;
		inta  	 : IN std_logic;
		intr	 : OUT std_logic);
END timer;

ARCHITECTURE Structure OF timer IS
	signal contador_ciclos : std_logic_vector(15 downto 0);
	signal contador_milisegundos : std_logic_vector(5 downto 0);
BEGIN

	PROCESS(boot,CLOCK_50)
	BEGIN
		if boot = '1' then
			contador_ciclos <= x"0000";
			contador_milisegundos <= "000000";
			intr <= '0';
		else
			if rising_edge(CLOCK_50) then
				contador_ciclos <= contador_ciclos + '1';
				
				if contador_ciclos = x"C350" then  --C350
					contador_ciclos <= x"0000"; -- tiempo de ciclo=20ns(50Mhz) 1ms=50000ciclos
					contador_milisegundos <= contador_milisegundos + '1';
					
					if contador_milisegundos = "110010" then  --110010
						contador_milisegundos <= "000000";
						intr <= '1';
					end if;
				end if;
				
				if inta = '1' then
					intr <= '0';
				end if;
			end if;
		end if;
	END PROCESS;

END Structure;