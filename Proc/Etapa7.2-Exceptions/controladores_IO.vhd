LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;

ENTITY controladores_IO IS
  PORT (boot : IN STD_LOGIC;
        CLOCK_50 : IN std_logic;
        addr_io : IN std_logic_vector(7 downto 0);
        wr_io : in std_logic_vector(15 downto 0);
        rd_io : out std_logic_vector(15 downto 0);
        wr_out : in std_logic;
        rd_in : in std_logic;
        led_verdes : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        led_rojos : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        visor0 : OUT STD_LOGIC_VECTOR(6 downto 0);
        visor1 : OUT STD_LOGIC_VECTOR(6 downto 0);
        visor2 : OUT STD_LOGIC_VECTOR(6 downto 0);
        visor3 : OUT STD_LOGIC_VECTOR(6 downto 0);
        switches : IN STD_LOGIC_VECTOR(7 downto 0);
        keys : IN STD_LOGIC_VECTOR(3 downto 0);
        ps2_clk : inout std_logic;
        ps2_data : inout std_logic;
        vga_cursor : out std_logic_vector(15 downto 0);
        vga_cursor_enable : out std_logic;
		intr : out std_logic;
		inta : in std_logic);
END controladores_IO;

ARCHITECTURE Structure OF controladores_IO IS

  COMPONENT driver7segmentos IS
    PORT( codigoCaracter : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
          bitsCaracter : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
  END COMPONENT;
  
  component keyboard_controller is
    port (clk : in STD_LOGIC;
          reset : in STD_LOGIC;
          ps2_clk : inout STD_LOGIC;
          ps2_data : inout STD_LOGIC;
          read_char : out STD_LOGIC_VECTOR (7 downto 0);
          clear_char : in STD_LOGIC;
          data_ready : out STD_LOGIC);
  end component;

  COMPONENT interrupt_controller IS
    PORT (inta	    : IN std_logic;
          key_intr    : IN std_logic;
          ps2_intr    : IN std_logic;
          switch_intr : IN std_logic;
          timer_intr  : IN std_logic;
          intr		: OUT std_logic;
          key_inta	: OUT std_logic;
          ps2_inta	: OUT std_logic;
          switch_inta	: OUT std_logic;
          timer_inta	: OUT std_logic;
          iid	 		: OUT std_logic_vector(7 downto 0));
  END COMPONENT;

  COMPONENT pulsadores IS
  PORT (boot	 : IN std_logic;
        clk     : IN std_logic;
        inta	 : IN std_logic;
        keys	 : IN std_logic_vector(3 downto 0);
        intr	 : OUT std_logic;
        read_key : OUT std_logic_vector(3 downto 0));
  END COMPONENT;

  COMPONENT interruptores IS
    PORT (boot	  : IN std_logic;
          clk     : IN std_logic;
          inta	  : IN std_logic;
          switches  : IN std_logic_vector(7 downto 0);
          intr	  : OUT std_logic;
          rd_switch : OUT std_logic_vector(7 downto 0));
  END COMPONENT;

  COMPONENT timer IS
    PORT (CLOCK_50 : IN std_logic;
          boot	 : IN std_logic;
          inta  	 : IN std_logic;
          intr	 : OUT std_logic);
  END COMPONENT;

  type REG_ARR is array (255 downto 0) of std_logic_vector(15 downto 0);
  signal ioreg : REG_ARR;
  signal v0_out, v1_out, v2_out, v3_out : std_LOGIC_VECTOR(6 downto 0);
  signal ps2_char : STD_LOGIC_VECTOR (7 downto 0);
  signal key_intr, ps2_intr, switch_intr, timer_intr : STD_LOGIC;
  signal key_inta, ps2_inta, switch_inta, timer_inta : STD_LOGIC;
	signal Iiid : STD_LOGIC_VECTOR(7 downto 0);
  signal key_data : std_logic_vector(3 downto 0);
  signal switch_data : std_logic_vector(7 downto 0);

  signal contador_ciclos : STD_LOGIC_VECTOR(15 downto 0) := x"0000";
  signal rd_io_tmp : STD_LOGIC_VECTOR(15 downto 0);

BEGIN
  
  intctrl0 : interrupt_controller PORT MAP (inta => inta,
                                            key_intr => key_intr,
                                            ps2_intr => ps2_intr,
                                            switch_intr => switch_intr,
                                            timer_intr => timer_intr,
                                            intr	=> intr,
                                            key_inta	=> key_inta,
                                            ps2_inta	=> ps2_inta,
                                            switch_inta	=> switch_inta,
                                            timer_inta	=> timer_inta,
                                            iid	=> Iiid);
  
  keys0 : pulsadores PORT MAP (boot => boot,
                               clk => CLOCK_50,
                               inta	=> key_inta,
                               keys	=> keys,
                               intr	=> key_intr,
                               read_key => key_data);

  switch0 : interruptores PORT MAP (boot	=> boot,
                                    clk => CLOCK_50,
                                    inta	=> switch_inta,
                                    switches => switches,
                                    intr	=> switch_intr,
                                    rd_switch => switch_data);

  kb0 : keyboard_controller PORT MAP (clk => CLOCK_50,
                                      reset => boot,
                                      ps2_clk => ps2_clk,
                                      ps2_data => ps2_data,
                                      read_char => ps2_char,
                                      clear_char => ps2_inta,
                                      data_ready => ps2_intr);

  timer0 : timer PORT MAP (CLOCK_50 => CLOCK_50,
                           boot	=> boot,
                           inta => timer_inta,
                           intr => timer_intr);

  v0: driver7segmentos PORT MAP (codigoCaracter => ioreg(10)(3 downto 0),
                                 bitsCaracter => v0_out);
  
  v1: driver7segmentos PORT MAP (codigoCaracter => ioreg(10)(7 downto 4),
                                 bitsCaracter => v1_out);
  
  v2: driver7segmentos PORT MAP (codigoCaracter => ioreg(10)(11 downto 8),
                                 bitsCaracter => v2_out);
  
  v3: driver7segmentos PORT MAP (codigoCaracter => ioreg(10)(15 downto 12),
                                 bitsCaracter => v3_out);
  
  vga_cursor <= x"0000";
  vga_cursor_enable <= '0';
  
  led_verdes <= ioreg(5)(7 downto 0);
  led_rojos <= ioreg(6)(7 downto 0);
  visor0 <= v0_out when ioreg(9)(0) = '1' else (others => '1');
  visor1 <= v1_out when ioreg(9)(1) = '1' else (others => '1');
  visor2 <= v2_out when ioreg(9)(2) = '1' else (others => '1');
  visor3 <= v3_out when ioreg(9)(3) = '1' else (others => '1');
  
  rd_io_tmp <= ioreg(to_integer(unsigned(addr_io))) when inta = '0' else x"00" & Iiid;
  rd_io <= rd_io_tmp when rd_in = '1' else (others => 'X');

  process (CLOCK_50)
    variable contador_milisegundos : STD_LOGIC_VECTOR(15 downto 0) := x"0000";
  begin
    if rising_edge(CLOCK_50) then
--      if ps2_clear = '1' then
--        ps2_clear <= '0';
--      end if
      ioreg(7) <= (15 downto 4 => '0') & key_data;
      ioreg(8) <= (15 downto 8 => '0') & switch_data;
      ioreg(15) <= (15 downto 8 => '0') & ps2_char;
--      ioreg(16) <= (15 downto 1 => '0') & ps2_intr;
      ioreg(20) <= contador_ciclos;
      
      contador_milisegundos := ioreg(21);
      
      if contador_ciclos = 0 then
        contador_ciclos <= x"C350"; -- tiempo de ciclo=20ns(50Mhz) 1ms=50000ciclos
        if contador_milisegundos > 0 then
          contador_milisegundos := contador_milisegundos-1;
          ioreg(21) <= contador_milisegundos;
        end if;
      else
        contador_ciclos <= contador_ciclos-1;
      end if;
      
      if wr_out = '1' then
--        if (unsigned(addr_io) = 16) then
--          ps2_clear <= '1';
--        els
        if (unsigned(addr_io) /= 7 and
               unsigned(addr_io) /= 8 and
               unsigned(addr_io) /= 15 and
               unsigned(addr_io) /= 20) then
          ioreg(to_integer(unsigned(addr_io))) <= wr_io;
        end if;
      end if;
    end if;
  end process;
END Structure;
