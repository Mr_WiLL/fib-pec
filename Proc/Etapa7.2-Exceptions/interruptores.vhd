LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY interruptores IS
  PORT (boot	  : IN std_logic;
		clk		  : IN std_logic;
		inta	  : IN std_logic;
		switches  : IN std_logic_vector(7 downto 0);
		intr	  : OUT std_logic;
		rd_switch : OUT std_logic_vector(7 downto 0));
END interruptores;

ARCHITECTURE Structure OF interruptores IS
  signal int : std_logic := '0';
  signal last_switches : std_logic_vector(7 downto 0);
BEGIN

  intr <= int;
  rd_switch <= last_switches;
  
  PROCESS (boot,clk)
  BEGIN
    if boot = '1' then
      last_switches <= switches;
      int <= '0';
    else 
      if rising_edge(clk) then
        if int = '1' and inta = '1' then
          int <= '0';
        end if;
        
        if last_switches /= switches and int = '0' then
          last_switches <= switches;
          int <= '1';
        end if;
      end if;
    end if;
  END PROCESS;
END Structure;