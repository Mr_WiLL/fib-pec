LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY sisa IS
    PORT (CLOCK_50  : IN    STD_LOGIC;
          SRAM_ADDR : out   std_logic_vector(17 downto 0);
          SRAM_DQ   : inout std_logic_vector(15 downto 0);
          SRAM_UB_N : out   std_logic;
          SRAM_LB_N : out   std_logic;
          SRAM_CE_N : out   std_logic := '1';
          SRAM_OE_N : out   std_logic := '1';
          SRAM_WE_N : out   std_logic := '1';
          SW        : in std_logic_vector(9 downto 9));
END sisa;

ARCHITECTURE Structure OF sisa IS

	COMPONENT proc IS
		 PORT (clk       : IN  STD_LOGIC;
				 boot      : IN  STD_LOGIC;
				 datard_m  : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
				 addr_m    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
				 data_wr   : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
				 wr_m      : OUT STD_LOGIC;
				 word_byte : OUT STD_LOGIC);
	END COMPONENT;

	COMPONENT MemoryController is
		 port (CLOCK_50  : in  std_logic;
				 addr      : in  std_logic_vector(15 downto 0);
				 wr_data   : in  std_logic_vector(15 downto 0);
				 rd_data   : out std_logic_vector(15 downto 0);
				 we        : in  std_logic;
				 byte_m    : in  std_logic;
				 SRAM_ADDR : out   std_logic_vector(17 downto 0);
				 SRAM_DQ   : inout std_logic_vector(15 downto 0);
				 SRAM_UB_N : out   std_logic;
				 SRAM_LB_N : out   std_logic;
				 SRAM_CE_N : out   std_logic := '1';
				 SRAM_OE_N : out   std_logic := '1';
				 SRAM_WE_N : out   std_logic := '1');
	end COMPONENT;
	
	SIGNAL clk_fmt : std_logic_vector (2 downto 0) := "000";
	SIGNAL wr_m_out, word_byte_out : std_logic;
	SIGNAL rd_data_out, data_wr_out, addr_m_out : std_logic_vector(15 downto 0);
	
BEGIN

	pro0 : proc PORT MAP (clk => clk_fmt(2),
								 boot => SW(9),
								 datard_m => rd_data_out,
								 addr_m => addr_m_out,
								 data_wr => data_wr_out,
								 wr_m => wr_m_out,
								 word_byte => word_byte_out);
								 
	mem0: MemoryController PORT MAP (CLOCK_50 => CLOCK_50,
										   	addr => addr_m_out,
											   wr_data => data_wr_out,
										      rd_data => rd_data_out,
											   we => wr_m_out,
											   byte_m => word_byte_out,
											   SRAM_ADDR => SRAM_ADDR,
											   SRAM_DQ => SRAM_DQ,
											   SRAM_UB_N => SRAM_UB_N,
											   SRAM_LB_N => SRAM_LB_N,
											   SRAM_CE_N => SRAM_CE_N,
											   SRAM_OE_N => SRAM_OE_N,
											   SRAM_WE_N => SRAM_WE_N);

	PROCESS (CLOCK_50)
	BEGIN
		if rising_edge(CLOCK_50) then
			clk_fmt <= clk_fmt + '1';
		end if;
	END PROCESS;
		
END Structure;