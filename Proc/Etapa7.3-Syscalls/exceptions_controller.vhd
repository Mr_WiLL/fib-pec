LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY exceptions_controller IS
  PORT (boot : IN std_logic;
        clk : IN std_logic;
        il_intr   : IN  STD_LOGIC;  -- e0
        inv_addr   : IN  STD_LOGIC;  -- e1
        of_fp   : IN  STD_LOGIC;  -- e2
        div_z_fp   : IN  STD_LOGIC;  -- e3
        div_z   : IN  STD_LOGIC;  -- e4
        m_TLB_ins   : IN  STD_LOGIC;  -- e6
        m_TLB_data   : IN  STD_LOGIC;  -- e7
        inv_page_TLB_ins   : IN  STD_LOGIC;  -- e8
        inv_page_TLB_data   : IN  STD_LOGIC;  -- e9
        prot_page_TLB_ins   : IN  STD_LOGIC;  -- e10
        prot_page_TLB_data   : IN  STD_LOGIC;  -- e11
        rd_page   : IN  STD_LOGIC;  -- e12
        prot_e   : IN  STD_LOGIC;  -- e13
        sys_call : IN STD_LOGIC;  -- e14
        int_rq     : IN STD_LOGIC;  -- e15
        exc_rq     : OUT STD_LOGIC := '0';  -- exception request
        eid	        : OUT std_logic_vector(3 downto 0));
END exceptions_controller;

ARCHITECTURE Structure OF exceptions_controller IS
    SIGNAL int_vec : std_logic_vector(15 downto 0);
    SIGNAL eid_code : std_logic_vector(15 downto 0);
    SIGNAL Iexc_rq : std_logic;
BEGIN
    exc_rq <= sys_call or prot_e or rd_page or prot_page_TLB_data or prot_page_TLB_ins or inv_page_TLB_data or
              inv_page_TLB_ins or m_TLB_data or m_TLB_ins or div_z or div_z_fp or of_fp or inv_addr or il_intr;

    --Iexc_rq <= '1' when (div_z = '1' or inv_addr = '1' or il_intr = '1') else '0';
    --with boot select exc_rq <= '0' when '1',
                               --Iexc_rq when others;

    int_vec <= int_rq & sys_call & prot_e & rd_page & prot_page_TLB_data & prot_page_TLB_ins & inv_page_TLB_data &
               inv_page_TLB_ins & m_TLB_data & m_TLB_ins & '0' & div_z & div_z_fp & of_fp & inv_addr & il_intr;

    with eid_code select eid <= x"0" when x"0001", -- e0
                                x"1" when x"0002", -- e1
                                x"2" when x"0004", -- e2
                                x"3" when x"0008", -- e3
                                x"4" when x"0010", -- e4
                                x"6" when x"0040", -- e6
                                x"7" when x"0080", -- e7
                                x"8" when x"0100", -- e8
                                x"9" when x"0200", -- e9
                                x"A" when x"0400", -- e10
                                x"B" when x"0800", -- e11
                                x"C" when x"1000", -- e12
                                x"D" when x"2000", -- e13
                                x"E" when x"4000", -- e14
                                x"F" when others;  -- e15

    PROCESS (clk)
    BEGIN
        if rising_edge(clk) then
            eid_code <= int_vec and std_logic_vector(unsigned(not(int_vec))+1);
            end if;
    END PROCESS;
END Structure;
