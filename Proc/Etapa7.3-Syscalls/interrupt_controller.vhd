LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY interrupt_controller IS
  PORT (inta	    : IN std_logic;
        key_intr    : IN std_logic;
        ps2_intr    : IN std_logic;
        switch_intr : IN std_logic;
        timer_intr  : IN std_logic;
        intr        : OUT std_logic;
        key_inta    : OUT std_logic;
        ps2_inta    : OUT std_logic;
        switch_inta : OUT std_logic;
        timer_inta  : OUT std_logic;
        iid	    : OUT std_logic_vector(7 downto 0));
END interrupt_controller;

ARCHITECTURE Structure OF interrupt_controller IS
  signal intr_vector, inta_vector : std_logic_vector(3 downto 0);
  signal iid_code : std_logic_vector(3 downto 0) := (others => '0');
BEGIN
  
  intr <= ps2_intr or switch_intr or key_intr or timer_intr;
  intr_vector <= ps2_intr & switch_intr & key_intr & timer_intr;

  timer_inta <= iid_code(0) and inta;
  key_inta <= iid_code(1) and inta;
  switch_inta <= iid_code(2) and inta;
  ps2_inta <= iid_code(3) and inta;

  with iid_code select iid <= x"00" when x"1",
                              x"01" when x"2",
                              x"02" when x"4",
                              x"03" when x"8",
                              x"FF" when others;

  PROCESS(inta)
  BEGIN
    if rising_edge(inta) then
      iid_code <= intr_vector and std_logic_vector(unsigned(not(intr_vector))+1);
    end if;
  END PROCESS;
END Structure;
