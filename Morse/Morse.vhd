LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY Morse IS
	PORT( SW : IN std_logic_vector(2 downto 0);
			KEY : IN std_logic_vector(1 downto 0);
			CLOCK_50 : IN std_logic;
			HEX0 : OUT std_logic_vector(6 downto 0);
			LEDR : OUT std_logic_vector(0 downto 0);
			LEDG : OUT std_logic_vector(0 downto 0));
END Morse;

ARCHITECTURE Structure OF Morse IS
	COMPONENT CharController IS
		PORT( char : IN std_logic_vector(13 downto 0);
				clk : IN std_logic;
				transmit : IN std_logic;
				eot : OUT std_logic := '0';
				txsig : OUT std_logic := '0'; -- transmission led
				nosig : OUT std_logic := '0'); -- idle led
	END COMPONENT;
	
	COMPONENT driver7Segmentos IS
		PORT( codigoCaracter : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
				bitsCaracter : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
	END COMPONENT;
	
	SIGNAL t_char : std_logic_vector(2 downto 0);
	SIGNAL char_code : std_logic_vector(13 downto 0);
	SIGNAL t_eot : std_logic;
	SIGNAL t_transmit : std_logic;
	SIGNAL transmit_code : std_logic_vector(2 downto 0);
	SIGNAL start : std_logic;
	SIGNAL abort : std_logic;
BEGIN
	Visor : driver7Segmentos 
		Port Map (codigoCaracter => SW,
					 bitsCaracter => HEX0);

	Transmitter : CharController
		Port Map (char => char_code,
				    clk => CLOCK_50,
					 transmit => t_transmit,
					 eot => t_eot,
					 txsig => LEDR(0),
					 nosig => LEDG(0));
	
	with t_char select char_code <= "01011100000000" when "000",
											  "01110101010000" when "001",
											  "01110101110100" when "010",
											  "01110101000000" when "011",
											  "01000000000000" when "100",
											  "01010111010000" when "101",
											  "01110111010000" when "110",
											  "01010101000000" when others;
	
	start <= not KEY(1);
	abort <= not KEY(0);
	transmit_code <= abort & start & t_eot;
	with transmit_code select t_transmit <= t_transmit when "000",
                                                   '1' when "010",
                                                   '1' when "011",
                                                   '0' when others;
																	
	PROCESS (KEY(1))
	BEGIN
		if falling_edge(KEY(1)) then
			t_char <= SW;
		end if;
	END PROCESS;

END Structure;