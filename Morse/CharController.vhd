LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY CharController IS
	PORT( char : IN std_logic_vector(13 downto 0);
			clk : IN std_logic;
			transmit : IN std_logic;
			eot : OUT std_logic := '0';
			txsig : OUT std_logic := '0'; -- transmission led
			nosig : OUT std_logic := '0'); -- idle led
END CharController;

ARCHITECTURE Structure OF CharController IS
	COMPONENT GenClock IS
		GENERIC( Halves : integer := 1);
		PORT( ClockIN : IN std_logic;
				ClockOUT : OUT std_logic);
	END COMPONENT;

	SIGNAL clk_fmt : std_logic := '0';
	SIGNAL cnt : integer := 13;
BEGIN
	nosig <= not transmit;
	txsig <= char(cnt) and transmit;
	
	Clock : GenClock
			  Generic Map ( Halves => 1)
			  Port Map( ClockIN => clk,
							ClockOUT => clk_fmt);
		
	PROCESS (clk_fmt)
	BEGIN
		if (rising_edge(clk_fmt)) then
			if transmit = '1' then
				if (cnt > 0 and cnt < 13 and char(cnt) = '0' and char(cnt-1) = '0') then
					cnt <= 13;
					eot <= '1';
				else 
					cnt <= cnt - 1;
					eot <= '0';
				end if;
			else
				cnt <= 13;
			end if;
		end if;
	END PROCESS;
END Structure;