LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY DemoAnd2_vhdl IS
	PORT(SW : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
		  LEDR : OUT STD_LOGIC_VECTOR(0 DOWNTO 0));
END DemoAnd2_vhdl;

ARCHITECTURE Structure OF DemoAnd2_vhdl IS
BEGIN
	LEDR(0) <= SW(0) and SW(1);
END Structure;