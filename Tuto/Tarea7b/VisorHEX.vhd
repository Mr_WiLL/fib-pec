LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY VisorHEX IS
	PORT( HEXvalue : IN std_logic_vector(15 downto 0);
			CODE0 : OUT std_logic_vector(6 downto 0);
			CODE1 : OUT std_logic_vector(6 downto 0);
			CODE2 : OUT std_logic_vector(6 downto 0);
			CODE3 : OUT std_logic_vector(6 downto 0));
END VisorHEX;

ARCHITECTURE Structure OF VisorHEX IS
COMPONENT driver7Segmentos IS
	PORT( codigoNum : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			bitsNum : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));
END COMPONENT;
BEGIN
	Visor0 : driver7Segmentos
		Port Map( codigoNum => HEXvalue(3 downto 0),
					 bitsNum => CODE0);
	Visor1 : driver7Segmentos
		Port Map( codigoNum => HEXvalue(7 downto 4),
					 bitsNum => CODE1);
	Visor2 : driver7Segmentos
		Port Map( codigoNum => HEXvalue(11 downto 8),
					 bitsNum => CODE2);
	Visor3 : driver7Segmentos
		Port Map( codigoNum => HEXvalue(15 downto 12),
					 bitsNum => CODE3);
END Structure;