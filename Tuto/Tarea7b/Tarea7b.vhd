LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY Tarea7b IS
	PORT( CLOCK_50 : IN std_logic;
			HEX0 : OUT std_logic_vector(6 downto 0);
			HEX1 : OUT std_logic_vector(6 downto 0);
			HEX2 : OUT std_logic_vector(6 downto 0);
			HEX3 : OUT std_logic_vector(6 downto 0));
END Tarea7b;

ARCHITECTURE Structure OF Tarea7b IS
	COMPONENT VisorHEX IS
		PORT( HEXvalue : IN std_logic_vector(15 downto 0);
				CODE0 : OUT std_logic_vector(6 downto 0);
				CODE1 : OUT std_logic_vector(6 downto 0);
				CODE2 : OUT std_logic_vector(6 downto 0);
				CODE3 : OUT std_logic_vector(6 downto 0));
	END COMPONENT;
	
	COMPONENT GenClock IS
		GENERIC( Seconds : integer := 1);
		PORT( ClockIN : IN std_logic;
				ClockOUT : OUT std_logic);
	END COMPONENT;
	
	SIGNAL clk : std_logic := '0';
	SIGNAL cnt : std_logic_vector(15 downto 0) := (others => '0');
BEGIN
	Visor : VisorHEX
			  Port Map( HEXvalue => cnt,
							CODE0 => HEX0,
							CODE1 => HEX1,
							CODE2 => HEX2,
							CODE3 => HEX3);
	Clock : GenClock
			  Generic Map( Seconds => 4)
			  Port Map( ClockIN => CLOCK_50,
							ClockOUT => clk);
							
	PROCESS (clk)
	BEGIN
		if rising_edge(clk) then
			cnt <= cnt + '1';
		end if;
	END PROCESS;
END Structure;